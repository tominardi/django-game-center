from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.utils import simplejson as json
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.core.urlresolvers import reverse

from games.models import Game, Engine, Process


def listing(request, page, engine, favorite):
    variables = {}

    if(engine is None):
        games_list = Game.objects.all()
    else:
        engine = Engine.objects.get(slug=engine)
        games_list = engine.game_set.all()
        if engine.background:
            variables['specific_background'] = engine.background.url
        variables['engine'] = engine
    paginator = Paginator(games_list, 10)

    try:
        posts = paginator.page(page)
    except PageNotAnInteger:
        if (engine is None):
            return redirect(reverse('games:list_games', None))
        else:
            return redirect(engine.get_absolute_url())
    except EmptyPage:
        if (engine is None):
            return redirect(reverse('games:list_games', None))
        else:
            return redirect(engine.get_absolute_url())

    engines = Engine.objects.all()

    variables['games'] = posts

    return render(request, 'listing.html', variables)


def single_game(request, slug):
    game = Game.objects.get(slug=slug)

    variables = {}
    variables['game'] = game
    variables['engine'] = game.engine
    if game.background:
        variables['specific_background'] = game.background.url

    return render(request, 'single.html', variables)


def stop_process():
    process_list = Process.objects.all()
    for process in process_list:
        #do kill command
        pass
    return True


def launch(request, game_slug):
    # stop current game if any
    stop_process()
    # get rom path
    # get emulator command
    # construct parameters
    # launch command
    # save pid
    return HttpResponse(json.dumps('{"status":"ok"}'),
                        content_type="application/json")


def stop(request):
    stop_process()
    return HttpResponse(json.dumps('{"status":"ok"}'),
                        content_type="application/json")
