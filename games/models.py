from django.db import models
from django.conf import settings
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from huron.utils.models import has_changed, RichTextField

from PIL import Image as PILImage
from PIL import ImageOps

from cStringIO import StringIO


class Engine(models.Model):
    """

    Engine are the emulators.

    Fields available:

    * name - CharField
    * slug - SlugField
    * command - CharField
    * parameters - CharField
    * extensions - CharField
    * image - ImageField
    * background - ImageField

    """
    name = models.CharField(_(u'name'), max_length=255)
    slug = models.SlugField(_(u'slug'), max_length=255)
    command = models.CharField(_(u'command'), max_length=255,
                               help_text=_(u'command used without parameters'))
    parameters = models.CharField(_(u'parameters'), max_length=255, blank=True)
    extensions = models.CharField(_(u'extensions'), max_length=255,
                                  help_text=_(u'roms extensions, comma separated'))
    image = models.ImageField(_(u'featured image'), upload_to='engines',
                               blank=True, null=True)
    background = models.ImageField(_(u'background image'),
                                   upload_to='engines/backgrounds', blank=True,
                                   null=True)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)
        verbose_name = _(u'engine')
        verbose_name_plural = _(u'engines')

    def get_absolute_url(self):
        return reverse('games:list_games_by_engine', None, [self.slug])

    def launch(self, rom):
        """

            Launch the game and store pid in a process.

            :param rom: the absolute path of the rom to launch
        """
        pass

    def save(self, *args, **kwargs):
        if has_changed(self, 'image'):
            if self.image:
                if hasattr(settings, 'IMAGE_ENGINE_WIDTH'):
                    width = settings.IMAGE_ENGINE_WIDTH
                else:
                    width = 220
                if hasattr(settings, 'IMAGE_ENGINE_HEIGHT'):
                    height = settings.IMAGE_ENGINE_HEIGHT
                else:
                    height = 160
                filename = self.slug
                image = PILImage.open(self.image.file)

                if image.mode not in ('L', 'RGB'):
                    image = image.convert('RGB')

                imagefit = ImageOps.fit(image, (width, height),
                                        PILImage.ANTIALIAS)
                temp = StringIO()
                imagefit.save(temp, 'jpeg')
                temp.seek(0)

                self.image.save(
                    filename+'.jpg',
                    SimpleUploadedFile('temp', temp.read()),
                    save=False)

        super(Engine, self).save()


class Game(models.Model):
    name = models.CharField(_(u'name'), max_length=255)
    slug = models.SlugField(_(u'slug'), max_length=255)
    rom = models.FileField(_(u'rom'), upload_to='games/roms')
    description = RichTextField(_(u'description'))
    image = models.ImageField(_(u'featured image'), upload_to='games/images',
                               blank=True, null=True)
    background = models.ImageField(_(u'background image'),
                                   upload_to='games/backgrounds', blank=True,
                                   null=True)
    favorite = models.BooleanField(_(u'favorite'))
    engine = models.ForeignKey(Engine)

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ('name',)
        verbose_name = _(u'game')
        verbose_name_plural = _(u'games')

    def get_absolute_url(self):
        return reverse('games:single_game', None, [self.slug])

    def save(self, *args, **kwargs):
        if has_changed(self, 'image'):
            if self.image:
                if hasattr(settings, 'IMAGE_ENGINE_WIDTH'):
                    width = settings.IMAGE_ENGINE_WIDTH
                else:
                    width = 220
                if hasattr(settings, 'IMAGE_ENGINE_HEIGHT'):
                    height = settings.IMAGE_ENGINE_HEIGHT
                else:
                    height = 160
                filename = self.slug
                image = PILImage.open(self.image.file)

                if image.mode not in ('L', 'RGB'):
                    image = image.convert('RGB')

                imagefit = ImageOps.fit(image, (width, height),
                                        PILImage.ANTIALIAS)
                temp = StringIO()
                imagefit.save(temp, 'jpeg')
                temp.seek(0)

                self.image.save(
                    filename+'.jpg',
                    SimpleUploadedFile('temp', temp.read()),
                    save=False)

        super(Game, self).save()


class Process(models.Model):
    name = models.CharField(_(u'name'), max_length=255)
    pid = models.IntegerField(_(u'pid'))

    def kill(self):
        """
            Kill process in host system and then delete self
        """
