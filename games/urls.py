# -*- coding: UTF-8 -*-
from django.conf.urls import patterns, url


urlpatterns = patterns('games.views',
                       url(r'^$', 'listing', {'page': 1, 'engine': None, 'favorite': False},
                           name="list_games"),
                       url(r'^page/(?P<page>\d+)/$', 'listing',
                           {'engine': None, 'favorite': False}, name="list_games_paged"),
                       url(r'^favorites/$', 'listing',
                           {'engine': None, 'page': 1, 'favorite': True},
                           name="list_favorites_games"),
                       url(r'^favorites/(?P<page>\d+)/$', 'listing',
                           {'engine': None, 'favorite': True},
                           name="list_favorites_games_paged"),
                       url(r'^(?P<slug>[-\w]+)/$', 'single_game',
                           name='single_game'),
                       url(r'^engine/(?P<engine>[-\w]+)/$', 'listing',
                           {'page': 1, 'favorite': False}, name="list_games_by_engine"),
                       url(r'^engine/(?P<engine>[-\w]+)/page/(?P<page>\d+)/$',
                           'listing', {'favorite': False},
                           name="list_games_by_engine_paged"),
                       url(r'^api/launch/(?P<game_slug>[-\w]+)/$', 'launch',
                           name="launch_game"),
                       url(r'^api/stop/$', 'stop', name="stop_game"),
                       )
