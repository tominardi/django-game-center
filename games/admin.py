from games.models import Engine, Game
from django.contrib import admin
from huron.utils.admin import CKEditorAdmin


class SluggedAdmin(CKEditorAdmin):
    prepopulated_fields = {'slug': ('name',)}

admin.site.register(Engine, SluggedAdmin)
admin.site.register(Game, SluggedAdmin)
