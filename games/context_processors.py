from games.models import Engine

def get_engines(request):
    datas = {}
    datas['engines'] = Engine.objects.all()
    return datas
