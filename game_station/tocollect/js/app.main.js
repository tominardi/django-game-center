jQuery(function($){
    // backstretch
    var wallpaper = "/static/img/wallpaper.jpg"
    if (window.hasOwnProperty('specificBackground')) {
        wallpaper = window.specificBackground
    }
    $.backstretch(wallpaper);
});
