from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render

def home(request):
    variables = {}

    variables['breadcrumbs'] = [{'name': _('Home')},]
    return render(request, 'index.html', variables)
